﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace RockstarApi.Test
{
    public class InMemoryContext : SqliteContext
    {
        private readonly SqliteConnection connection;

        public InMemoryContext(SqliteConnection connection)
        {
            this.connection = connection;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(this.connection);
        }
    }
}