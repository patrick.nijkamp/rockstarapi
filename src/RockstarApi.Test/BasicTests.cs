using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RockstarApi.Api;
using RockstarApi.Controllers;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases;
using RockstarApi.Infra.Entities;
using RockstarApi.Infra.Persisters;
using RockstarApi.Infra.Repositories;
using RockstarApi.Infra.Requests;
using RockstarApi.Infra.Responses;
using RockstarApi.Test.Mocks;

namespace RockstarApi.Test
{
    [TestClass]
    public class CreateControllerTests : MockedDbTest
    {
        [TestMethod]
        public async Task CreateArtistTest()
        {
            ArtistController artistController = this.GetArtistControllerMock();

            string artistName = "Test Artist";

            await artistController.CreateArtist(new CreateArtistRequest()
            {
                Name = artistName
            });

            int artistCount = await this.DbContext.Set<ArtistEntity>().CountAsync();

            Assert.AreEqual(1, artistCount);

            ArtistEntity? artist = await this.DbContext.Set<ArtistEntity>()
                .SingleOrDefaultAsync(a => a.Name == artistName);
        }

        [TestMethod]
        public async Task ReadArtistTest()
        {
            ArtistController artistController = this.GetArtistControllerMockWithMoqRepo();

            var result = await artistController.ReadArtists(new ReadArtistsRequest()
            {
                Name = "Test Artist"
            }) as JsonStatusResult<List<ArtistResponseModel>>;
            
            Assert.IsNotNull(result);
            
            Assert.AreEqual("Test Artist", result.Response.First().Name);
        }
        
        [TestMethod]
        public async Task CreateSongTest()
        {
            ArtistController artistController = this.GetArtistControllerMock();
            SongController songController = this.GetSongControllerMock();

            string artistName = "Test Artist";

            await artistController.CreateArtist(new CreateArtistRequest()
            {
                Name = artistName
            });

            ArtistEntity artist = await this.DbContext.Set<ArtistEntity>().SingleAsync();

            await songController.CreateSong(new CreateSongRequest()
            {
                Name = "Test song",
                Artist = artist.Guid
            });

            int songCount = await this.DbContext.Set<SongEntity>().CountAsync();

            Assert.AreEqual(1, songCount);
        }

        [TestMethod]
        public async Task CreateSongWithoutNameTest()
        {
            SongController songController = this.GetSongControllerMock();

            await songController.CreateSong(new CreateSongRequest()
            {
                Artist = Guid.NewGuid()
            });

            int songCount = await this.DbContext.Set<SongEntity>().CountAsync();

            Assert.AreEqual(0, songCount);
        }
        
        [TestMethod]
        public async Task CreateSongWithoutValidArtistTest()
        {
            SongController songController = this.GetSongControllerMock();

            await songController.CreateSong(new CreateSongRequest()
            {
                Name = "Test",
                Artist = Guid.NewGuid()
            });

            int songCount = await this.DbContext.Set<SongEntity>().CountAsync();

            Assert.AreEqual(0, songCount);
        }

        private ArtistController GetArtistControllerMock()
        {
            return new ArtistController(
                new CreateArtistUseCase(
                    new ArtistPersister(this.DbContext)
                ),
                new ReadArtistsUseCase(
                    new ArtistRepository(this.DbContext)
                ),
                new UpdateArtistUseCase(
                    new ArtistPersister(this.DbContext)
                ),
                new DeleteArtistUseCase()
            );
        }
        
        private ArtistController GetArtistControllerMockWithMoqRepo()
        {
            var mock = new Mock<IArtistRepository>();
            mock.Setup(m => m.GetArtistsByName("Test Artist")).Returns(Task.FromResult(new List<Artist>()
            {
                new Artist()
                {
                    ArtistId = 1,
                    Guid = Guid.NewGuid(),
                    Name = "Test Artist",
                    Songs = new List<Song>()
                }
            }));
            
            return new ArtistController(
                new CreateArtistUseCase(
                    new ArtistPersister(this.DbContext)
                ),
                new ReadArtistsUseCase(
                    mock.Object
                ),
                new UpdateArtistUseCase(
                    new ArtistPersister(this.DbContext)
                ),
                new DeleteArtistUseCase()
            );
        }

        private SongController GetSongControllerMock()
        {
            return new SongController(
                new CreateSongUseCase(
                    new SongPersister(this.DbContext), new ArtistRepository(this.DbContext)
                ),
                new ReadSongsUseCase(
                    new SongRepository(this.DbContext)
                ),
                new UpdateSongUseCase(
                    new SongPersister(this.DbContext)
                ),
                new DeleteSongUseCase()
            );
        }
    }
}