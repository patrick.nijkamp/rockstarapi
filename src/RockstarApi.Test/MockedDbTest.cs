﻿using Microsoft.Data.Sqlite;

namespace RockstarApi.Test.Mocks
{
    public class MockedDbTest
    {
        private const string InMemoryConnectionString = "DataSource=:memory:";
        private readonly SqliteConnection connection;

        protected readonly InMemoryContext DbContext;

        protected MockedDbTest()
        {
            connection = new SqliteConnection(InMemoryConnectionString);
            connection.Open();

            DbContext = new InMemoryContext(connection);
            DbContext.Database.EnsureCreated();
        }

        public void Dispose()
        {
            connection.Close();
        }
    }
}