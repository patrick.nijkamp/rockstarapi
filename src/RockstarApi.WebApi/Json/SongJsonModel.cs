﻿namespace RockstarApi.Json
{
    public class SongJsonModel
    {
        public string Name { get; set; } = string.Empty;
        
        public int? Year { get; set; }
        
        public string Shortname { get; set; } = string.Empty;
        
        public int? Bpm { get; set; }
        
        public int? Duration { get; set; }
        
        public string? Genre { get; set; }
        
        public string? SpotifyId { get; set; }
        
        public string? Album { get; set; }
        
        public string? Artist { get; set; }
    }
}