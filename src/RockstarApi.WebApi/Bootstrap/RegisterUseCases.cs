﻿using Microsoft.Extensions.DependencyInjection;
using RockstarApi.Domain.UseCases;

namespace RockstarApi.Bootstrap
{
    public static class RegisterUseCases
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<CreateArtistUseCase>();
            services.AddScoped<CreateSongUseCase>();
            services.AddScoped<DeleteArtistUseCase>();
            services.AddScoped<DeleteSongUseCase>();
            services.AddScoped<ReadArtistsUseCase>();
            services.AddScoped<ReadSongsUseCase>();
            services.AddScoped<SeedUseCase>();
            services.AddScoped<UpdateArtistUseCase>();
            services.AddScoped<UpdateSongUseCase>();
        }
    }
}