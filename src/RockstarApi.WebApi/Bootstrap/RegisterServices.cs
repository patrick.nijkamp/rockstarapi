﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace RockstarApi.Bootstrap
{
    public static class RegisterServices
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<DbContext, SqliteContext>();
        }
    }
}