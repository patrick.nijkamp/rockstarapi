﻿using Microsoft.Extensions.DependencyInjection;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Infra.Repositories;

namespace RockstarApi.Bootstrap
{
    public class RegisterRepositories
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IArtistRepository, ArtistRepository>();
            services.AddScoped<ISongRepository, SongRepository>();
        }
    }
}