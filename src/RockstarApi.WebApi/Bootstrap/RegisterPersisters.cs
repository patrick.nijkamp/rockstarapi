﻿using Microsoft.Extensions.DependencyInjection;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Infra.Persisters;

namespace RockstarApi.Bootstrap
{
    public class RegisterPersisters
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IArtistPersister, ArtistPersister>();
            services.AddScoped<ISongPersister, SongPersister>();
        }
    }
}