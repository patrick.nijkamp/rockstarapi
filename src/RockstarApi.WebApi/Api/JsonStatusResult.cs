﻿using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RockstarApi.Api
{
    public class JsonStatusResult<TResponseObject> : IActionResult
    {
        public TResponseObject Response { get; set; }

        [JsonIgnore]
        private HttpStatusCode StatusCode { get; set; }

        public JsonStatusResult(TResponseObject response, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            this.Response = response;
            this.StatusCode = statusCode;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var serializerOptions = new JsonSerializerOptions();
            serializerOptions.Converters.Add(new JsonStringEnumConverter(null));
            serializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            serializerOptions.IgnoreNullValues = true;

            var objectResult = new JsonResult(this.Response)
            {
                StatusCode = (int) this.StatusCode,
                SerializerSettings = serializerOptions
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }
}