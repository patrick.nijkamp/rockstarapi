﻿# Artist/Song API
This API was build by me to show what I can do. I could only spend 4 hours so not everything is done. I couldn't finish the delete controllers and because of time issues I didn't manage to get everything perfect.

# How the project was built
The project was built with regards to the Clean Architecture. There is some leakage here and there because of time limits. The "Domain" project contains (or should contain) the business logic. It contains the interfaces for the repositories/persisters and also the domain models and the business usecases with their respective input.

The "Infra" project contains everything that connects the Domain layer with the WebApi layer. Entities, mappers, persister and repository implementations (I have chosen EFCore) and the request / response models.

The "WebApi" project contains a very basic .NET api setup. I tried to keep the default files as clean as possible. Couldn't get it as clean as I wanted to because of, you guessed it, time limits. Didn't want to spend too much time cleaning up :)

The "Test" project contains some basic tests. I didn't have the time to write all the test or really meaningful tests. So they are just a proof of ability that's all.

# How to run the project
* Clone this awesome repository
* Open solution
* Restore nuget packages
* Open nuget package console
* Run command: cd .\src\RockstarApi.WebApi
* Run command: dotnet ef database update
* Run the project (I don't use IIS Express so for me it's port 5001)
* GET the https://localhost:5001/seed endpoint to seed the database with the JSON files (you can run it from the Swagger UI)
* Visit https://localhost:5001/swagger for basic docs (didn't have time to add all specific requests/responses but it works!)
* https://localhost:5001/json/artists.json for the artists JSON file
* https://localhost:5001/json/songs.json for the songs JSON file