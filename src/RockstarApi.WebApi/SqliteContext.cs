﻿using Microsoft.EntityFrameworkCore;
using RockstarApi.Infra.Entities;

namespace RockstarApi
{
    public class SqliteContext : DbContext
    {
        public DbSet<ArtistEntity> Artists { get; set; } = null!;

        public DbSet<SongEntity> Songs { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=RockstarApiDb.db;", x =>
            {
            });
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArtistEntity>().ToTable("Artists");
            modelBuilder.Entity<SongEntity>().ToTable("Songs");
        }
    }
}