﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RockstarApi.Api;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases;
using RockstarApi.Domain.UseCases.Input;
using RockstarApi.Infra.Mappers;
using RockstarApi.Infra.Requests;
using RockstarApi.Infra.Responses;
using Swashbuckle.AspNetCore.Annotations;

namespace RockstarApi.Controllers
{
    public class ArtistController
    {
        private readonly CreateArtistUseCase createArtistUseCase;
        private readonly ReadArtistsUseCase readArtistsUseCase;
        private readonly UpdateArtistUseCase updateArtistUseCase;
        private readonly DeleteArtistUseCase deleteArtistUseCase;

        public ArtistController(
            CreateArtistUseCase createArtistUseCase, 
            ReadArtistsUseCase readArtistsUseCase, 
            UpdateArtistUseCase updateArtistUseCase, 
            DeleteArtistUseCase deleteArtistUseCase
        )
        {
            this.createArtistUseCase = createArtistUseCase;
            this.readArtistsUseCase = readArtistsUseCase;
            this.updateArtistUseCase = updateArtistUseCase;
            this.deleteArtistUseCase = deleteArtistUseCase;
        }

        [HttpPost]
        [SwaggerResponse(200, "The created object", typeof(ArtistResponseModel))]
        [Route("artist")]
        public async Task<IActionResult> CreateArtist(CreateArtistRequest request)
        {
            Artist result = await this.createArtistUseCase.ExecuteAsync(new CreateArtistInput()
            {
                Name = request.Name
            });

            return new JsonStatusResult<ArtistResponseModel>(result.ToResponseModel());
        }
        
        [HttpGet]
        [SwaggerResponse(200, "The created object", typeof(List<ArtistResponseModel>))]
        [Route("/artists")]
        public async Task<IActionResult> ReadArtists([FromQuery] ReadArtistsRequest request)
        {
            List<Artist> result = await this.readArtistsUseCase.ExecuteAsync(new ReadArtistsInput()
            {
                Name = request.Name
            });
            
            return new JsonStatusResult<List<ArtistResponseModel>>(result.ToResponseModelList(false));
        }
        
        [HttpPatch]
        [SwaggerResponse(200, "The created object", typeof(ArtistResponseModel))]
        [SwaggerResponse(400, "Invalid GUID / Artist not found", typeof(ErrorResponse))]
        [Route("artist/{guid}")]
        public async Task<IActionResult> UpdateArtist([Required] Guid? guid, [FromBody] UpdateArtistRequest request)
        {
            if (guid == null || guid == Guid.Empty)
            {
                return new JsonStatusResult<ErrorResponse>(new ErrorResponse()
                {
                    ErrorAlias = "INVALID_GUID",
                    ErrorMessage = "The GUID you specified is invalid"
                }, HttpStatusCode.BadRequest);
            }
            
            Artist? result = await this.updateArtistUseCase.ExecuteAsync(new UpdateArtistInput()
            {
                Guid = guid.GetValueOrDefault(),
                Name = request.Name
            });

            if (result == null)
            {
                return new JsonStatusResult<ErrorResponse>(new ErrorResponse()
                {
                    ErrorAlias = "INVALID_ARTIST",
                    ErrorMessage = "Artist not found"
                }, HttpStatusCode.BadRequest);
            }
            
            return new JsonStatusResult<ArtistResponseModel>(result.ToResponseModel(false));
        }
    }
}