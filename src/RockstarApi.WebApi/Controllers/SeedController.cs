﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using RockstarApi.Api;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases;
using RockstarApi.Domain.UseCases.Input;
using RockstarApi.Infra.Responses;
using RockstarApi.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace RockstarApi.Controllers
{
    [ApiController]
    public class SeedController : Controller
    {
        private static List<SongJsonModel>? jsonSongList = null;
        private readonly SeedUseCase seedUseCase;
        private readonly IHostEnvironment environment;

        public SeedController(SeedUseCase seedUseCase, IHostEnvironment environment)
        {
            this.seedUseCase = seedUseCase;
            this.environment = environment;
        }

        [HttpGet]
        [SwaggerResponse(200, "True if database was successfully seeded!", typeof(bool))]
        [Route("/seed")]
        public async Task<JsonStatusResult<bool>> Get()
        {
            bool result = await seedUseCase.ExecuteAsync(new SeedInput()
            {
                Artists = await this.GetArtistsWithSongs(),
            });

            return new JsonStatusResult<bool>(result, result == true ? HttpStatusCode.OK : HttpStatusCode.InternalServerError);
        }

        private async Task<List<Artist>> GetArtistsWithSongs()
        {
            string artistJsonPath = Path.Combine(this.environment.ContentRootPath, "Json", "artists.json");

            if (!System.IO.File.Exists(artistJsonPath))
                throw new Exception($"Could not find {artistJsonPath}");

            string artistContent = await System.IO.File.ReadAllTextAsync(artistJsonPath);
            List<ArtistJsonModel> artists = JsonSerializer.Deserialize<List<ArtistJsonModel>>(artistContent);

            var result = new List<Artist>();

            foreach (ArtistJsonModel artistJsonModel in artists)
            {
                result.Add(new Artist()
                    {
                        Name = artistJsonModel.Name,
                        Songs = await this.GetSongs(artistJsonModel.Name)
                    }
                );
            }

            return result;
        }

        private async Task<List<Song>> GetSongs(string artistName)
        {
            // Cache the songlist in a local static variable so that we can iterate the list much faster
            if (jsonSongList == null)
            {
                string songJsonPath = Path.Combine(this.environment.ContentRootPath, "Json", "songs.json");

                if (!System.IO.File.Exists(songJsonPath))
                    throw new Exception($"Could not find {songJsonPath}");

                string songContent = await System.IO.File.ReadAllTextAsync(songJsonPath);
                jsonSongList = JsonSerializer.Deserialize<List<SongJsonModel>>(songContent);
            }

            return jsonSongList.Where(song => song.Artist == artistName).Select(x => new Song()
            {
                Name = x.Name,
                Year = x.Year,
                Shortname = x.Shortname,
                Bpm = x.Bpm,
                Duration = x.Duration,
                Genre = x.Genre,
                SpotifyId = x.SpotifyId,
                Album = x.Album
            }).ToList();
        }
    }
}