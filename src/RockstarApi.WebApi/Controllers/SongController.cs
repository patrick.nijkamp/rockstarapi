﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RockstarApi.Api;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases;
using RockstarApi.Domain.UseCases.Input;
using RockstarApi.Infra.Mappers;
using RockstarApi.Infra.Requests;
using RockstarApi.Infra.Responses;
using Swashbuckle.AspNetCore.Annotations;

namespace RockstarApi.Controllers
{
    public class SongController
    {
        private readonly CreateSongUseCase createSongUseCase;
        private readonly ReadSongsUseCase readSongsUseCase;
        private readonly UpdateSongUseCase updateSongUseCase;
        private readonly DeleteSongUseCase deleteSongUseCase;

        public SongController(
            CreateSongUseCase createSongUseCase, 
            ReadSongsUseCase readSongsUseCase, 
            UpdateSongUseCase updateSongUseCase, 
            DeleteSongUseCase deleteSongUseCase
        )
        {
            this.createSongUseCase = createSongUseCase;
            this.readSongsUseCase = readSongsUseCase;
            this.updateSongUseCase = updateSongUseCase;
            this.deleteSongUseCase = deleteSongUseCase;
        }

        [HttpPost]
        [SwaggerResponse(200, "The created object", typeof(SongResponseModel))]
        [SwaggerResponse(400, "Invalid artist supplied", typeof(ErrorResponse))]
        [Route("song")]
        public async Task<IActionResult> CreateSong(CreateSongRequest request)
        {
            Song? result = await this.createSongUseCase.ExecuteAsync(new CreateSongInput()
            {
                Name = request.Name,
                Year = request.Year,
                Shortname = request.Shortname,
                Bpm = request.Bpm,
                Duration = request.Duration,
                Genre = request.Genre,
                SpotifyId = request.SpotifyId,
                Album = request.Album,
                Artist = request.Artist.GetValueOrDefault(),
            });

            if (result == null)
            {
                return new JsonStatusResult<ErrorResponse>(new ErrorResponse()
                {
                    ErrorAlias = "INVALID_SONG_CREATE_REQUEST",
                    ErrorMessage = "Song could not be created, are you sure the artist exists?"
                }, HttpStatusCode.BadRequest);
            }

            return new JsonStatusResult<SongResponseModel>(result.ToResponseModel());
        }
        
        [HttpGet]
        [SwaggerResponse(200, "A list of songs", typeof(List<SongResponseModel>))]
        [Route("/songs")]
        public async Task<IActionResult> ReadSongs([FromQuery] ReadSongsRequest request)
        {
            List<Song> result = await this.readSongsUseCase.ExecuteAsync(new ReadSongsInput()
            {
                Genre = request.Genre
            });
            
            return new JsonStatusResult<List<SongResponseModel>>(result.ToResponseModelList());
        }
        
        [HttpPatch]
        [SwaggerResponse(200, "The created object", typeof(SongResponseModel))]
        [SwaggerResponse(400, "Invalid GUID supplied", typeof(ErrorResponse))]
        [Route("song/{guid}")]
        public async Task<IActionResult> UpdateSong([Required] Guid? guid, [FromBody] UpdateSongRequest request)
        {
            if (guid == null || guid == Guid.Empty)
            {
                return new JsonStatusResult<ErrorResponse>(new ErrorResponse()
                {
                    ErrorAlias = "INVALID_GUID",
                    ErrorMessage = "The GUID you specified is invalid"
                });
            }
            
            Song? result = await this.updateSongUseCase.ExecuteAsync(new UpdateSongInput()
            {
                Guid = guid.GetValueOrDefault(),
                Name = request.Name,
                Year = request.Year,
                Shortname = request.Shortname,
                Bpm = request.Bpm,
                Duration = request.Duration,
                Genre = request.Genre,
                SpotifyId = request.SpotifyId,
                Album = request.Album,
            });

            if (result == null)
            {
                if (result == null)
                {
                    return new JsonStatusResult<ErrorResponse>(new ErrorResponse()
                    {
                        ErrorAlias = "INVALID_SONG",
                        ErrorMessage = "Song not found"
                    });
                }
            }
            
            return new JsonStatusResult<SongResponseModel>(result.ToResponseModel(false));
        }
    }
}