﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RockstarApi.Migrations
{
    public partial class UpdatedArtistFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Artists",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "StartedPlaying",
                table: "Artists",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Artists");

            migrationBuilder.DropColumn(
                name: "StartedPlaying",
                table: "Artists");
        }
    }
}
