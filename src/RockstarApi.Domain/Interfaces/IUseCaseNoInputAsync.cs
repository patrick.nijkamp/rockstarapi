﻿using System.Threading.Tasks;

namespace RockstarApi.Domain.Interfaces
{
    public interface IUseCaseNoInputAsync<TOutput>
    {
        Task<TOutput> ExecuteAsync();
    }
}