﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RockstarApi.Domain.Models;

namespace RockstarApi.Domain.Interfaces
{
    public interface ISongRepository
    {
        Task<List<Song>> GetSongsByGenre(string genre);
    }
}