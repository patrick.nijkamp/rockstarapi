﻿using System.Threading.Tasks;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.Interfaces
{
    public interface ISongPersister
    {
        Task<Song> AddSong(Song song);
        
        Task<Song?> UpdateSong(UpdateSongInput input);
    }
}