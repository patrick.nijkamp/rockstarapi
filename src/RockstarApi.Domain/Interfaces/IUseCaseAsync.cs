﻿using System.Threading.Tasks;

namespace RockstarApi.Domain.Interfaces
{
    public interface IUseCaseAsync<TInput, TOutput>
    {
        Task<TOutput> ExecuteAsync(TInput input);
    }
}