﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.Interfaces
{
    public interface IArtistPersister
    {
        Task AddArtistsWithSongs(List<Artist> artists);

        Task<Artist> AddArtist(Artist artist);

        Task<Artist?> UpdateArtist(UpdateArtistInput input);
    }
}