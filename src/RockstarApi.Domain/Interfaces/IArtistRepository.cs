﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RockstarApi.Domain.Models;

namespace RockstarApi.Domain.Interfaces
{
    public interface IArtistRepository
    {
        Task<Artist?> GetArtistByGuid(Guid guid);

        Task<List<Artist>> GetArtistsByName(string name);
    }
}