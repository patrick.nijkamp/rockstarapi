﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.UseCases
{
    public class SeedUseCase : IUseCaseAsync<SeedInput, bool>
    {
        private readonly IArtistPersister artistPersister;
        private readonly ISongPersister songPersister;

        public SeedUseCase(IArtistPersister artistPersister, ISongPersister songPersister)
        {
            this.artistPersister = artistPersister;
            this.songPersister = songPersister;
        }
        
        public async Task<bool> ExecuteAsync(SeedInput input)
        {
            // Filter songs by metal Genre
            List<Artist> artists = input.Artists.Where(
                x => x.Songs.Any(s => s.Genre.ToLower().Contains("metal"))
            ).ToList();
            
            // Add GUIDs
            artists = artists.Select(artist =>
            {
                artist.Guid = Guid.NewGuid();

                artist.Songs = artist.Songs.Select(song =>
                {
                    song.SongId = null;
                    song.Guid = Guid.NewGuid();
                    return song;
                }).ToList();
                
                return artist;
            }).ToList();

            try
            {
                await this.artistPersister.AddArtistsWithSongs(artists);
            }
            catch
            {
                return false;
            }
            

            return true;
        }
    }
}