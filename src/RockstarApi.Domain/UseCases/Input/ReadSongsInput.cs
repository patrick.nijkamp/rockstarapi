﻿namespace RockstarApi.Domain.UseCases.Input
{
    public class ReadSongsInput
    {
        public string Genre { get; set; } = null!;
    }
}