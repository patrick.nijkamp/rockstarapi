﻿namespace RockstarApi.Domain.UseCases.Input
{
    public class CreateArtistInput
    {
        public string Name { get; set; }
    }
}