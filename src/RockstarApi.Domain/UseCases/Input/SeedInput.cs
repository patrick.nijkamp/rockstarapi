﻿using System.Collections.Generic;
using RockstarApi.Domain.Models;

namespace RockstarApi.Domain.UseCases.Input
{
    public class SeedInput
    {
        public List<Artist> Artists { get; set; }
    }
}