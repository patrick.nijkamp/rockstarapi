﻿using System;

namespace RockstarApi.Domain.UseCases.Input
{
    public class UpdateArtistInput
    {
        public Guid Guid { get; set; }
        
        public string Name { get; set; }
    }
}