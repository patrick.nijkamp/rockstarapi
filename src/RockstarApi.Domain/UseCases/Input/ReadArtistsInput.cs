﻿namespace RockstarApi.Domain.UseCases.Input
{
    public class ReadArtistsInput
    {
        public string Name { get; set; } = null!;
    }
}