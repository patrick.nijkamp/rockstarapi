﻿using System.Threading.Tasks;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.UseCases
{
    public class UpdateArtistUseCase : IUseCaseAsync<UpdateArtistInput, Artist?>
    {
        private readonly IArtistPersister artistPersister;

        public UpdateArtistUseCase(IArtistPersister artistPersister)
        {
            this.artistPersister = artistPersister;
        }
        
        public async Task<Artist?> ExecuteAsync(UpdateArtistInput input)
        {
            Artist? updatedArtist = await this.artistPersister.UpdateArtist(input);

            return updatedArtist;
        }
    }
}