﻿using System.Threading.Tasks;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.UseCases
{
    public class UpdateSongUseCase : IUseCaseAsync<UpdateSongInput, Song>
    {
        private readonly ISongPersister songPersister;

        public UpdateSongUseCase(ISongPersister songPersister)
        {
            this.songPersister = songPersister;
        }
        
        public async Task<Song?> ExecuteAsync(UpdateSongInput input)
        {
            Song? updatedSong = await this.songPersister.UpdateSong(input);

            return updatedSong;
        }
    }
}