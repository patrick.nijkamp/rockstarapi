﻿using System.Threading.Tasks;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.UseCases
{
    public class CreateArtistUseCase : IUseCaseAsync<CreateArtistInput, Artist>
    {
        private readonly IArtistPersister artistPersister;

        public CreateArtistUseCase(IArtistPersister artistPersister)
        {
            this.artistPersister = artistPersister;
        }
        
        public async Task<Artist> ExecuteAsync(CreateArtistInput input)
        {
            return await this.artistPersister.AddArtist(new Artist()
            {
                Name = input.Name
            });
        }
    }
}