﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.UseCases.Input;
using RockstarApi.Domain.Models;

namespace RockstarApi.Domain.UseCases
{
    public class ReadArtistsUseCase : IUseCaseAsync<ReadArtistsInput, List<Artist>>
    {
        private readonly IArtistRepository artistRepository;

        public ReadArtistsUseCase(IArtistRepository artistRepository)
        {
            this.artistRepository = artistRepository;
        }
        
        public async Task<List<Artist>> ExecuteAsync(ReadArtistsInput input)
        {
            return await this.artistRepository.GetArtistsByName(input.Name);
        }
    }
}