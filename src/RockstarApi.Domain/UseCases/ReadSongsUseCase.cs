﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.UseCases
{
    public class ReadSongsUseCase : IUseCaseAsync<ReadSongsInput, List<Song>>
    {
        private readonly ISongRepository songRepository;

        public ReadSongsUseCase(ISongRepository songRepository)
        {
            this.songRepository = songRepository;
        }
        
        public async Task<List<Song>> ExecuteAsync(ReadSongsInput input)
        {
            return await this.songRepository.GetSongsByGenre(input.Genre);
        }
    }
}