﻿using System.Threading.Tasks;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Domain.UseCases
{
    public class CreateSongUseCase : IUseCaseAsync<CreateSongInput, Song?>
    {
        private readonly ISongPersister songPersister;
        private readonly IArtistRepository artistRepository;

        public CreateSongUseCase(ISongPersister songPersister, IArtistRepository artistRepository)
        {
            this.songPersister = songPersister;
            this.artistRepository = artistRepository;
        }
        
        public async Task<Song?> ExecuteAsync(CreateSongInput input)
        {
            Artist? artist = await this.artistRepository.GetArtistByGuid(input.Artist);
            
            if (artist == null)
                return null;

            Song song = await this.songPersister.AddSong(new Song()
            {
                Name = input.Name,
                Year = input.Year,
                Shortname = input.Shortname,
                Bpm = input.Bpm,
                Duration = input.Duration,
                Genre = input.Genre,
                SpotifyId = input.SpotifyId,
                Album = input.Album,
                Artist = artist
            });

            return song;
        }
    }
}