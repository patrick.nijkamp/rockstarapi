﻿using System;
using System.Collections.Generic;

namespace RockstarApi.Domain.Models
{
    public class Artist
    {
        public int? ArtistId { get; set; }
        
        public Guid? Guid { get; set; }
        
        public string Name { get; set; }
        
        public List<Song> Songs { get; set; }
    }
}