﻿using System;

namespace RockstarApi.Domain.Models
{
    public class Song
    {
        public int? SongId { get; set; }
        
        public Guid? Guid { get; set; }
        
        public string Name { get; set; }
        
        public int? Year { get; set; }
        
        public string? Shortname { get; set; }
        
        public int? Bpm { get; set; }
        
        public int? Duration { get; set; }
        
        public string? Genre { get; set; }
        
        public string? SpotifyId { get; set; }
        
        public string? Album { get; set; }

        public Artist? Artist { get; set; } = null!;
    }
}