﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Infra.Entities;
using RockstarApi.Infra.Mappers;
using RockstarApi.Domain.Models;

namespace RockstarApi.Infra.Repositories
{
    public class ArtistRepository : IArtistRepository
    {
        private readonly DbContext context;

        public ArtistRepository(DbContext context)
        {
            this.context = context;
        }
        public async Task<Artist?> GetArtistByGuid(Guid guid)
        {
            ArtistEntity artistEntity =
                await this.context.Set<ArtistEntity>().AsNoTracking().FirstOrDefaultAsync(artist => artist.Guid == guid);

            return artistEntity?.ToDomainModel();
        }

        public async Task<List<Artist>> GetArtistsByName(string name)
        {
            List<ArtistEntity> list = await this.context.Set<ArtistEntity>().Where(
                artist => artist.Name.ToLower().Contains(name.ToLower())
            ).ToListAsync();

            return list.ToDomainModelList();
        }
    }
}