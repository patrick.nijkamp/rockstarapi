﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Domain.Models;
using RockstarApi.Infra.Entities;
using RockstarApi.Infra.Mappers;

namespace RockstarApi.Infra.Repositories
{
    public class SongRepository : ISongRepository
    {
        private readonly DbContext context;

        public SongRepository(DbContext context)
        {
            this.context = context;
        }
        
        public async Task<List<Song>> GetSongsByGenre(string genre)
        {
            List<SongEntity> list = await this.context.Set<SongEntity>()
                .Include(x => x.Artist)
                .Where(song => song.Genre != null && song.Genre.ToLower().Contains(genre.ToLower()))
                .ToListAsync();

            return list.ToDomainModelList();
        }
    }
}