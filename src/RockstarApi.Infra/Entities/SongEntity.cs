﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RockstarApi.Infra.Entities
{
    public class SongEntity
    {
        [Key]
        public int? SongId { get; set; }
        
        public Guid? Guid { get; set; }

        public string Name { get; set; } = null!;
        
        public int? Year { get; set; }
        
        public string? Shortname { get; set; }
        
        public int? Bpm { get; set; }
        
        public int? Duration { get; set; }
        
        public string? Genre { get; set; }
        
        public string? SpotifyId { get; set; }
        
        public string? Album { get; set; }
        
        public int? ArtistId { get; set; }

        [ForeignKey("ArtistId")]
        public ArtistEntity Artist { get; set; } = null!;
    }
}