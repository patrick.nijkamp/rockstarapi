﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RockstarApi.Infra.Entities {
    public class ArtistEntity
    {
        [Key]
        public int? ArtistId { get; set; }
        
        public Guid? Guid { get; set; }
        
        public string Name { get; set; }
        
        public List<SongEntity>? Songs { get; set; } = new List<SongEntity>();
    }
}