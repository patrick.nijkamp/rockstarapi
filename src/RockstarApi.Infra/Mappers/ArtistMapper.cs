﻿using System;
using System.Collections.Generic;
using System.Linq;
using RockstarApi.Infra.Entities;
using RockstarApi.Infra.Responses;
using DomainArtist = RockstarApi.Domain.Models.Artist;
using EntityArtist = RockstarApi.Infra.Entities.ArtistEntity;

namespace RockstarApi.Infra.Mappers
{
    public static class ArtistMapper
    {
        public static ArtistResponseModel ToResponseModel(this DomainArtist source, bool mapSongs = true)
        {
            return new ArtistResponseModel()
            {
                Guid = source.Guid,
                Name = source.Name,
                Songs = mapSongs ? source.Songs.ToResponseModelList(false) : null
            };
        }

        public static List<ArtistResponseModel> ToResponseModelList(this List<DomainArtist> source, bool mapSongs = true) =>
            source.Select(x => x.ToResponseModel(mapSongs)).ToList();

        public static EntityArtist ToEntity(this DomainArtist source, bool mapSongs = true)
        {
            return new EntityArtist()
            {
                ArtistId = source.ArtistId,
                Guid = source.Guid,
                Name = source.Name,
                Songs = mapSongs ? source.Songs.ToEntityList(false) : new List<SongEntity>()
            };
        }
        
        public static List<EntityArtist> ToEntityList(this List<DomainArtist> source, bool mapSongs = true) =>
            source.Select(x => x.ToEntity(mapSongs)).ToList();

        public static DomainArtist ToDomainModel(this EntityArtist source, bool mapSongs = true)
        {
            return new DomainArtist()
            {
                ArtistId = source.ArtistId,
                Guid = source.Guid,
                Name = source.Name,
                Songs = mapSongs ? source.Songs.ToDomainModelList(false) : new List<Domain.Models.Song>()
            };
        }
        
        public static List<DomainArtist> ToDomainModelList(this List<EntityArtist> source, bool mapSongs = true) =>
            source.Select(x => x.ToDomainModel(mapSongs)).ToList();
    }
}