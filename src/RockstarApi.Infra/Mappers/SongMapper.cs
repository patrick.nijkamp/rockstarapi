﻿using System;
using System.Collections.Generic;
using System.Linq;
using RockstarApi.Infra.Entities;
using RockstarApi.Domain.Models;
using RockstarApi.Infra.Responses;
using DomainSong = RockstarApi.Domain.Models.Song;
using EntitySong = RockstarApi.Infra.Entities.SongEntity;

namespace RockstarApi.Infra.Mappers
{
    public static class SongMapper
    {
        public static SongResponseModel ToResponseModel(this DomainSong source, bool mapArtist = true)
        {
            return new SongResponseModel()
            {
                Guid = source.Guid,
                Name = source.Name,
                Year = source.Year,
                Shortname = source.Shortname,
                Bpm = source.Bpm,
                Duration = source.Duration,
                Genre = source.Genre,
                SpotifyId = source.SpotifyId,
                Album = source.Album,
                Artist = mapArtist ? source.Artist?.ToResponseModel(false) : null,
            };
        }
        
        public static List<SongResponseModel> ToResponseModelList(this List<DomainSong> source, bool mapArtist = true) =>
            source.Select(x => x.ToResponseModel(mapArtist)).ToList();

        public static EntitySong ToEntity(this DomainSong source, bool mapArtist = true)
        {
            return new SongEntity()
            {
                SongId = source.SongId,
                Guid = source.Guid,
                Name = source.Name,
                Year = source.Year,
                Shortname = source.Shortname,
                Bpm = source.Bpm,
                Duration = source.Duration,
                Genre = source.Genre,
                SpotifyId = source.SpotifyId,
                Album = source.Album,
                ArtistId = source.Artist?.ArtistId
            };
        }
        
        public static List<EntitySong> ToEntityList(this List<DomainSong> source, bool mapArtist = true) =>
            source.Select(x => x.ToEntity(mapArtist)).ToList();

        public static DomainSong ToDomainModel(this EntitySong source, bool mapArtist = true)
        {
            return new DomainSong()
            {
                SongId = source.SongId,
                Guid = source.Guid,
                Name = source.Name,
                Year = source.Year,
                Shortname = source.Shortname,
                Bpm = source.Bpm,
                Duration = source.Duration,
                Genre = source.Genre,
                SpotifyId = source.SpotifyId,
                Album = source.Album,
                Artist = mapArtist ? source.Artist.ToDomainModel(false) : new Artist(),   
            };
        }
        
        public static List<DomainSong> ToDomainModelList(this List<EntitySong> source, bool mapArtist = true) =>
            source.Select(x => x.ToDomainModel(mapArtist)).ToList();
    }
}