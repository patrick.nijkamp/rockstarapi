﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RockstarApi.Infra.Responses
{
    public class SongResponseModel
    {
        public Guid? Guid { get; set; }

        public string Name { get; set; } = null!;
        
        public int? Year { get; set; }
        
        public string? Shortname { get; set; }
        
        public int? Bpm { get; set; }
        
        public int? Duration { get; set; }
        
        public string? Genre { get; set; }
        
        public string? SpotifyId { get; set; }
        
        public string? Album { get; set; }

        public ArtistResponseModel? Artist { get; set; }
    }
}