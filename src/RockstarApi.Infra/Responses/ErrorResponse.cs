﻿namespace RockstarApi.Infra.Responses
{
    public class ErrorResponse
    {
        public string ErrorAlias { get; set; }
        
        public string ErrorMessage { get; set; }
    }
}