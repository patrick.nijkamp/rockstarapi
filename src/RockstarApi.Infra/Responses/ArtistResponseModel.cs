﻿using System;
using System.Collections.Generic;

namespace RockstarApi.Infra.Responses
{
    public class ArtistResponseModel
    {
        public Guid? Guid { get; set; }
        
        public string Name { get; set; }
        
        public List<SongResponseModel>? Songs { get; set; } = new List<SongResponseModel>();
    }
}