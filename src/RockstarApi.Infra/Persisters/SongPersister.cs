﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Infra.Entities;
using RockstarApi.Infra.Mappers;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Infra.Persisters
{
    public class SongPersister : ISongPersister
    {
        private readonly DbContext context;

        public SongPersister(DbContext context)
        {
            this.context = context;
        }
        
        public async Task<Song> AddSong(Song song)
        {
            SongEntity entity = song.ToEntity();
            entity.Guid = Guid.NewGuid();

            await this.context.Set<SongEntity>().AddAsync(entity);
            await this.context.SaveChangesAsync();

            Song songModel = entity.ToDomainModel(false);
            songModel.Artist = song.Artist;

            return songModel;
        }

        public async Task<Song?> UpdateSong(UpdateSongInput input)
        {
            SongEntity? entity = await this.context.Set<SongEntity>()
                .FirstOrDefaultAsync(song => song.Guid == input.Guid);

            if (entity == null)
                return null;

            entity.Name = input.Name ?? entity.Name;
            entity.Year = input.Year ?? entity.Year;
            entity.Shortname = input.Shortname ?? entity.Shortname;
            entity.Bpm = input.Bpm ?? entity.Bpm;
            entity.Duration = input.Duration ?? entity.Duration;
            entity.Genre = input.Genre ?? entity.Genre;
            entity.SpotifyId = input.SpotifyId ?? entity.SpotifyId;
            entity.Album = input.Album ?? entity.Album;

            await this.context.SaveChangesAsync();

            return entity.ToDomainModel(false);
        }
    }
}