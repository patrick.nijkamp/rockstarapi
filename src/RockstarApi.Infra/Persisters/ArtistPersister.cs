﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RockstarApi.Domain.Interfaces;
using RockstarApi.Infra.Entities;
using RockstarApi.Infra.Mappers;
using RockstarApi.Domain.Models;
using RockstarApi.Domain.UseCases.Input;

namespace RockstarApi.Infra.Persisters
{
    public class ArtistPersister : IArtistPersister
    {
        private readonly DbContext context;

        public ArtistPersister(DbContext context)
        {
            this.context = context;
        }
        
        public async Task AddArtistsWithSongs(List<Artist> artists)
        {
            List<ArtistEntity> entities = artists.ToEntityList();
            
            await this.context.Set<ArtistEntity>().AddRangeAsync(entities);
            await this.context.SaveChangesAsync();
            
            artists = entities.ToDomainModelList();
        }

        public async Task<Artist> AddArtist(Artist artist)
        {
            ArtistEntity entity = artist.ToEntity(false);
            entity.Guid = Guid.NewGuid();

            await this.context.Set<ArtistEntity>().AddAsync(entity);
            await this.context.SaveChangesAsync();

            return entity.ToDomainModel(false);
        }

        public async Task<Artist?> UpdateArtist(UpdateArtistInput input)
        {
            ArtistEntity? entity = await this.context.Set<ArtistEntity>()
                .FirstOrDefaultAsync(artist => artist.Guid == input.Guid);

            if (entity == null)
                return null;

            entity.Name = input.Name;

            await this.context.SaveChangesAsync();

            return entity.ToDomainModel();
        }
    }
}