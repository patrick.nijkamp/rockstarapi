﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RockstarApi.Infra.Requests
{
    public class CreateSongRequest
    {
        [Required]
        public string Name { get; set; } = null!;
        
        public int? Year { get; set; }
        
        public string? Shortname { get; set; }
        
        public int? Bpm { get; set; }
        
        public int? Duration { get; set; }
        
        public string? Genre { get; set; }
        
        public string? SpotifyId { get; set; }
        
        public string? Album { get; set; }
        
        [Required]
        public Guid? Artist { get; set; }
    }
}