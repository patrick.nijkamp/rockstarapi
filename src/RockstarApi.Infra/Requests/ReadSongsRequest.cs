﻿using System.ComponentModel.DataAnnotations;

namespace RockstarApi.Infra.Requests
{
    public class ReadSongsRequest
    {
        [Required]
        public string Genre { get; set; } = null!;
    }
}