﻿using System.ComponentModel.DataAnnotations;

namespace RockstarApi.Infra.Requests
{
    public class ReadArtistsRequest
    {
        [Required]
        public string Name { get; set; }
    }
}