﻿using System.ComponentModel.DataAnnotations;

namespace RockstarApi.Infra.Requests
{
    public class CreateArtistRequest
    {
        [Required]
        public string Name { get; set; } = null!;
    }
}