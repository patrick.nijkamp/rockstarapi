﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RockstarApi.Infra.Requests
{
    public class UpdateArtistRequest
    {
        [Required]
        public string Name { get; set; } = null!;
    }
}